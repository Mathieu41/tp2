from boite import *
from volume import *

def test_boite_base():
	b=Box()
	b.open()
	b.add("truc1")
	b.add("truc2")
	b.add("truc3")
	assert "truc1" in b
	assert "truc2" in b
	b.remove("truc2")
	assert "truc2" not in b
	assert b.is_open() == True
	b.close()
	assert b.is_open() == False
	b.open()
	assert b.is_open() == True
	b.close()
	assert b.add("lol") == False
	assert b.action_look() == "la boite est fermé"
	b.open()
	assert b.action_look() == "la boite contient: truc1 et truc3"
	assert b.get_capacity()== None
	b.set_capacity(5)
	assert b.get_capacity() == 5
	
def test_boite2():
	b=Box()
	b.open()
	a=Thing(4,name="test")
	c=Thing(6)
	assert b.has_room_for(a) == True
	assert b.has_room_for(c) == True
	b.set_capacity(4)
	assert b.has_room_for(c) == False
	assert b.action_add(a) == True
	assert b.get_capacity() == 0
	assert b.action_add(c) == False
	b.set_capacity(None)
	assert b.action_add(a) == True
	assert b.get_capacity() == None
	assert b.find("test") == a
	assert b.find(c) == None
	
	
def test_Thing():
	a=Thing(3,name="truc a")
	assert a.getVolume()==3
	assert a.has_name("lol") == False
	assert a.has_name("truc a") == True
	b=Thing(3,name="test2")
	c=Thing(4)
	
	
