from volume import *

class Box(object):
    def __init__(self, is_open=True, Capacity=None):
            self.__contents=[]
            self.__etat=is_open
            self.__capacite=Capacity

    def __contains__(self,machin):
            return machin in self.__contents

    def __repr__(self):
            if not self.is_open():
                    return "la boite est fermé"
            else:
                    return "<capacite:%s,%s>" % (self.__capacite, self.__contents)

    def add(self,truc):
        if not self.is_open():
                return False
        else:
                self.__contents.append(truc)

    def remove(self,machin):
            self.__contents=[x for x in self.__contents if x != machin]

    def is_open(self):
            return self.__etat

    def open(self):
            self.__etat=True

    def close(self):
            self.__etat=False

    def action_look(self):
            if not self.is_open():
                    return "la boite est fermé"
            else:
                    return "la boite contient: "+" et ".join(self.__contents)

    def set_capacity(self,capacite):
            self.__capacite=capacite

    def get_capacity(self):
            return self.__capacite

    def has_room_for(self,objet):
            if self.get_capacity()==None:
                    return True
            elif objet.getVolume() > self.get_capacity():
                    return False
            else:
                    return True

    def action_add(self,objet):
            if self.has_room_for(objet) and self.is_open():
                    self.add(objet)
                    if self.get_capacity()!=None:
                            self.set_capacity(self.get_capacity()-objet.getVolume())
                    return True
            else:
                    return False

    def find(self,nom):
            for elem in self.__contents:
                    if elem.has_name(nom):
                            return elem
            else:
                    return None


	
	
		
		
        
